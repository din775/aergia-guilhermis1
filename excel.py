from openpyxl import Workbook
from openpyxl.compat import range
from openpyxl.utils import get_column_letter
from openpyxl.styles import PatternFill

wb = Workbook()


ws1 = wb.active
ws1.title = "range names"

for row in range(0, len(range(501))):
    ws1.append([row])

ws1['A1'].fill = PatternFill(start_color="FFC7CE", end_color="FFC7CE", fill_type = "solid")

wb.save(filename = dest_filename)