﻿$server = Read-Host "Please enter the server to connect"
$login = Read-Host "Please enter your login"
$pass = Read-Host -assecurestring "Please enter your password"
$pass = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($pass))
$day = Get-Date -UFormat "%d"
$month = Get-Date -UFormat "%m" 
$year = Get-Date -UFormat "%Y"
$hour = Get-Date -UFormat "%H"
$hourM = ((get-date).AddHours(-1)).ToString("HH")


net use \\$server /user:lmcorp\$login $pass
$input_path = "\\$server\lmcti\Data\URALog\$year\$month\$day\errorlog$year$month$day.txt"
$output_file = "C:\Users\guilherme.fagundes.NSXBR\Desktop\OSC$server_$year$month$day.txt"

$regex = ‘(\[+[0-9\:]+\])+(\[+[a-z]+\])+(\[+[a-z\s]+\])+(\[+[a-z]+\])+(\[+[a-z0-9]+\])+\s+((AlarmID|State:+\s+Blocked)+(.*))’


select-string -Path $input_path -Pattern $regex -AllMatches | % { $_.Matches } | % { $_.Value } > $output_file

write-host "$input_path"