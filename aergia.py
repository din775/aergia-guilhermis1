from jira import JIRA
import pprint
import datetime
import re
from pyzabbix import ZabbixAPI
from openpyxl import Workbook
from openpyxl.formatting import Rule
from openpyxl.styles import Font
from openpyxl.styles.colors import Color
from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.styles.differential import DifferentialStyle
from openpyxl.formatting.rule import ColorScaleRule, CellIsRule, FormulaRule
from openpyxl.worksheet.table import Table, TableStyleInfo
# from openpyxl.styles import Alignment
import pprint

# The hostname at which the Zabbix web interface is available
ZABBIX_SERVER = 'https://zabbix.mutantbr.com/zabbix'

zapi = ZabbixAPI(ZABBIX_SERVER)

# Login to the Zabbix API


options = {
    'server': 'https://sd.mutantbr.com'
    }

wb = Workbook()
ws1 = wb.active
ws1.title = "Gerencia de alarmes"

dest_filename = r'path\aergia-guilhermis\gerencia_test.xlsx'
agora = datetime.datetime.now()
dt2 = datetime.timedelta(hours=4)
menos = agora - dt2
menos2 = menos.timestamp()
issuen = r'\b' + re.escape(r'[A-Z]+-+[0-9])\w+') + r'\b'

headerFill = PatternFill(start_color='4B72BD', end_color='4B72BD', fill_type='solid')
altaRule = PatternFill(start_color='ff704d', end_color='ff704d', fill_type='solid')
desastreRule = PatternFill(start_color='ff0000', end_color='ff0000', fill_type='solid')

ws1.conditional_formatting.add('D1:D100',
             FormulaRule(formula=['$D1:$D100 = "Alta"'], stopIfTrue=False, fill=altaRule, font = Font(color = "FFFFFF")))
ws1.conditional_formatting.add('D1:D100',
             FormulaRule(formula=['$D1:$D100 = "Desastre"'], stopIfTrue=False, fill=desastreRule, font = Font(color = "FFFFFF")))


ws1.cell(row=1, column=1).value = 'Servidor'
ws1.cell(row=1, column=2).value = 'Nome do Alarme'
ws1.cell(row=1, column=3).value = 'Idade do Alarme'
ws1.cell(row=1, column=4).value = 'Severidade'
ws1.cell(row=1, column=5).value = 'Alarme esta oscilando'
ws1.cell(row=1, column=6).value = 'JIRA'
ws1.cell(row=1, column=7).value = 'Responsável'
ws1.cell(row=1, column=8).value = 'Área Responsável'
ws1.cell(row=1, column=9).value = 'Status do chamado'
ws1.cell(row=1, column=10).value = 'Previsão de solução (Data e hora)'
ws1.cell(row=1, column=11).value = 'Status do alarme'
ws1.cell(row=1, column=12).value = 'Novo prazo'


# Get a list of all issues (AKA tripped triggers)
triggers = zapi.trigger.get(only_true=1,
                            skipDependent=1,
                            monitored=1,
                            active=1,
                            output='extend',
                            expandDescription=1,
                            selectHosts=['host'],
                            min_severity=4,
                            lastChangeTill=menos2,
                            )

# Do another query to find out which issues are Unacknowledged
unack_triggers = zapi.trigger.get(only_true=1,
                                  skipDependent=1,
                                  monitored=1,
                                  active=1,
                                  output='extend',
                                  expandDescription=1,
                                  selectHosts=['host'],
                                  withLastEventUnacknowledged=1,
                                  sortfield=['lastchange'],
                                  )
unack_trigger_ids = [t['triggerid'] for t in unack_triggers]
for t in triggers:
    t['unacknowledged'] = True if t['triggerid'] in unack_trigger_ids \
        else False

list_issues=[]
#Print a list containing only "tripped" triggers
for t in triggers:
    if int(t['value']) == 1:
        print("{0} - {1}  {2}".format(t['hosts'][0]['host'],
                                     t['description'],
                                     '----(Não reconhecido)' if t['unacknowledged'] else '')                          
              )
        eventz = zapi.event.get(objectids=t['triggerid'],#objectids = trigger ID
                        acknowledged=True,
                        select_acknowledges=['message'])
        issue = eventz[-1].get('acknowledges')[-1].get('message')

    if int(t['priority'])==4:
        priorita = 'Alta'
    else:
        priorita = 'Desastre'
    list_issues.append(issue)
    horita = datetime.datetime.fromtimestamp(int(t['lastchange']))
    horita2 = agora - horita
    
    ws1.append([t['hosts'][0]['host'],t['description'],horita2, priorita])
    print (horita2)


indices = [i for i, x in enumerate(list_issues) if re.match(r'([A-Z]+-+[0-9])\w+', x)]
f = 1
for i in indices:

        try:
                f = f+1
                issue_comment = jira.comments(list_issues[i])
                last_comment = jira.comment(list_issues[i], issue_comment[-1])
                ws1.cell(row=f, column=9).value = last_comment.body
                ws1.cell(row=f, column=6).value = list_issues[i]
                ws1.cell(row=f, column=7).value = jira.issue(list_issues[i]).fields.assignee.displayName

        except:
                print('algo aconteceu, verifique isso por favor: '+list_issues[i])

tab = Table(displayName="Table1", ref="A1:L50")
style = TableStyleInfo(name="TableStyleMedium9", showFirstColumn=False,
                       showLastColumn=False, showRowStripes=True, showColumnStripes=False)
tab.tableStyleInfo = style
ws1.add_table(tab)

wb.save(filename = dest_filename)